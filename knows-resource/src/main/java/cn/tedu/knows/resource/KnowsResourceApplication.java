package cn.tedu.knows.resource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
// 下面注解表示启动项目时,注册到配置好的Nacos配置中心中
@EnableDiscoveryClient   //EDC
public class KnowsResourceApplication {

    public static void main(String[] args) {
        SpringApplication.run(KnowsResourceApplication.class, args);
    }

}
