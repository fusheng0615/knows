package cn.tedu.knows.portal;

import cn.tedu.knows.portal.mapper.AnswerMapper;
import cn.tedu.knows.portal.mapper.TagMapper;
import cn.tedu.knows.portal.model.Answer;
import cn.tedu.knows.portal.model.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.List;

// 测试类添加注解
@SpringBootTest
public class TestTag {

    @Test
    public void tag(){
        Tag tag=new Tag();
        tag.setId(21);
        tag.setName("微服务");
        tag.setCreateby("tom");
        tag.setCreatetime(LocalDateTime.now());

        // 链式set赋值
        Tag t=new Tag()
                .setId(22)
                .setName("高并发")
                .setCreateby("jerry")
                .setCreatetime(LocalDateTime.now());

    }

    @Autowired
    TagMapper tagMapper;
    @Test
    public void add(){
        // 链式set赋值
        Tag t=new Tag()
                .setName("高并发")
                .setCreateby("jerry")
                .setCreatetime(LocalDateTime.now());
        int num=tagMapper.insert(t);
        System.out.println(num);
    }


    @Autowired
    AnswerMapper answerMapper;

    @Test
    public void getAns(){
        List<Answer> answers=
                answerMapper.findAnswersByQuestionId(149);
        for(Answer a: answers){
            System.out.println(a);
        }
    }












}
