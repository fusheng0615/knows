package cn.tedu.knows.portal;

import cn.tedu.knows.portal.mapper.ClassroomMapper;
import cn.tedu.knows.portal.mapper.QuestionMapper;
import cn.tedu.knows.portal.model.Classroom;
import cn.tedu.knows.portal.model.Question;
import cn.tedu.knows.portal.service.IUserService;
import cn.tedu.knows.portal.vo.RegisterVo;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.api.R;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class WrapperTest {

    // 根据邀请码查询班级信息
    // 如果写sql语句:
    // select * from classroom where invite_code='JSD2001-706246'
    // 如果使用QueryWrapper进行查询 代码如下
    @Autowired
    ClassroomMapper classroomMapper;
    @Test
    public void query(){
        // 我们实例化一个QueryWrapper的对象
        // 这个对象其实就是代表查询的条件,泛型是实体类的类型
        QueryWrapper<Classroom> query=new QueryWrapper<>();
        // 设置查询条件 query.eq([列名],[值])
        query.eq("invite_code","JSD2001-70624");
        // 按QueryWrapper对象设置好的条件进行查询的操作
        // selectOne方法只支持最多返回1行数据,否则报错,返回值是实体类型
        Classroom classroom=classroomMapper.selectOne(query);

        System.out.println(classroom);
    }

    @Autowired
    IUserService userService;
    @Test
    public void add(){
        RegisterVo registerVo=new RegisterVo();
        registerVo.setPhone("13033012345");
        registerVo.setNickname("大龙");
        registerVo.setInviteCode("JSD2001-706246");
        registerVo.setPassword("123456");
        userService.registerStudent(registerVo);
        System.out.println("ok");
    }


    @Autowired
    QuestionMapper questionMapper;
    @Test
    public void teacherList(){
        List<Question> list=questionMapper
                        .findTeacherQuestions(5);
        for (Question q: list){
            System.out.println(q);
        }

    }




}
