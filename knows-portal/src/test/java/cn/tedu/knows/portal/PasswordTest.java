package cn.tedu.knows.portal;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootTest
public class PasswordTest {

    // Bcrypt加密对象
    PasswordEncoder encoder=new BCryptPasswordEncoder();

    // 执行加密测试
    @Test
    public void test(){
        String str="123456";
        // 利用加密对象将str字符串加密为pwd
        String pwd=encoder.encode(str);
        // 输出加密之后的字符串内容
        System.out.println(pwd);
        //$2a$10$B5Ba4G77NuxAcRJ/iucipOaXjc/3uranz.lMW008IVxRdGBATv8d2
    }


    // 执行验证测试
    @Test
    public void match(){

        // 下面的方法验证一个字符串是否匹配一个加密结果
        // 返回boolean类型
        boolean b=encoder.matches("123456",
                "$2a$10$B5Ba4G77NuxAcRJ/iucipOaXjc/3uranz.lMW008IVxRdGBATv8d2");
        System.out.println("匹配结果:"+b);
    }




}
