let userApp = new Vue({
    el:"#userApp",
    data:{
        user:{}
    },
    methods:{
        loadUserVo:function(){
            axios({
                url:"/v1/users/me",
                method:"get"
            }).then(function(response){
                userApp.user=response.data;
            })
        }
    },
    created:function(){
        // 页面加载完毕时运行的方法
        this.loadUserVo();
    }
})