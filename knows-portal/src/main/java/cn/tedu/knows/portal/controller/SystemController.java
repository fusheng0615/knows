package cn.tedu.knows.portal.controller;

import cn.tedu.knows.portal.exception.ServiceException;
import cn.tedu.knows.portal.service.IUserService;
import cn.tedu.knows.portal.vo.RegisterVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

@RestController
// lombok提供的一个记录日志用的注解
// 一旦在类上添加@Slf4j,这个类的方法中就可以使用log对象记录日志
@Slf4j
public class SystemController {

    @Autowired
    private IUserService userService;

    @PostMapping("/register")
    public String register(
            // RegisterVo参数前添加@Validated表示开启服务器端验证功能
            // 当控制器方法运行之前,SpringValidation框架会按RegisterVo
            // 中编写的规则进行验证
            @Validated RegisterVo registerVo,
            // 这个参数必须紧随RegisterVo之后
            // result中包含RegisterVo的验证结果
            BindingResult result) {
        //利用日志对象,将接收到的信息输出到控制台
        log.debug("接收到用户信息:{}", registerVo);
        if (result.hasErrors()) {
            String msg = result.getFieldError().
                    getDefaultMessage();
            // 返回错误信息
            return msg;
        }

        userService.registerStudent(registerVo);
        return "ok";

    }

    // 获取application.properties配置的信息

    @Value("${knows.resource.path}")
    private File resourcePath;

    @Value("${knows.resource.host}")
    private String resourceHost;


    @PostMapping("/upload/file")
    //MultipartFile是SpringMvc框架提供的类型,专门用于接收上传的文件
    // imageFile表单中文件域name一致
    public String uploadFile(MultipartFile imageFile) throws IOException {

        // 根据日期获得path路径
        String path= DateTimeFormatter.ofPattern("yyyy/MM/dd")
                        .format(LocalDate.now());
        // path:  2021/11/03
        // 确定上传路径
        File folder=new File(resourcePath,path);
        // 创建文件夹
        folder.mkdirs();//mkdirssssss

        // 随机文件名
        String filename=imageFile.getOriginalFilename();//原始文件名
        //  xx.xx.jpg
        //  012345678
        String ext=filename.substring(filename
                                        .lastIndexOf("."));
        // ext:  .jpg
        String name= UUID.randomUUID().toString()+ext;
        // name:  [UUID].jpg   ajsdhfjkahsdkf.jpg
        // 确定要上传的路径
        File file=new File(folder,name);
        log.debug("上传的路径为:{}",file.getAbsolutePath());

        // 执行上传
        imageFile.transferTo(file);

        // 回显的关键,是上传控制返回通过静态资源服务器访问资源的路径
        //http://localhost:8899/2021/11/03/6dd-ecf91c532c4c.jpg
        //  resourceHost          path           name
        String url=resourceHost+"/"+path+"/"+name;
        log.debug("生成的Url:{}",url);
        // 返回url,以实现页面回显上传的图片
        return url;

    }






}










