package cn.tedu.knows.portal.mapper;

import cn.tedu.knows.portal.model.RolePermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
* <p>
    *  Mapper 接口
    * </p>
*
* @author tedu.cn
* @since 2021-10-27
*/
    @Repository
    public interface RolePermissionMapper extends BaseMapper<RolePermission> {

    }
