package cn.tedu.knows.portal.mapper;

import cn.tedu.knows.portal.model.Question;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tedu.cn
 * @since 2021-10-27
 */
@Repository
public interface QuestionMapper extends BaseMapper<Question> {

    // 根据用户id查询问题数
    @Select("select count(*) from question where user_id=#{id}")
    int countQuestionsByUserId(Integer userId);


    // 查询讲师问题列表的方法
    @Select("SELECT q.* FROM\n" +
            "question q \n" +
            "LEFT join user_question uq ON q.id=uq.question_id\n" +
            "WHERE uq.user_id=#{id} OR q.user_id=#{id}\n" +
            "ORDER BY q.createtime desc")
    List<Question> findTeacherQuestions(Integer userId);


    // 修改问题状态的sql语句
    @Update("update question set status=#{status} " +
            " where id=#{questionId}")
    int updateStatus(@Param("status") Integer status,
                     @Param("questionId") Integer questionId);


}







