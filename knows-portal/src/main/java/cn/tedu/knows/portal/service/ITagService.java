package cn.tedu.knows.portal.service;

import cn.tedu.knows.portal.model.Tag;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tedu.cn
 * @since 2021-10-27
 */
public interface ITagService extends IService<Tag> {

    // 查询所有标签对象的方法
    List<Tag> getTags();

    // 获得包含所有标签的Map对象的方法
    Map<String,Tag> getTagMap();

}







