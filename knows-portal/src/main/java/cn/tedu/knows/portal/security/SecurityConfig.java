package cn.tedu.knows.portal.security;

import cn.tedu.knows.portal.service.impl.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

// 表示当前配置类是配置Spring框架的
@Configuration
// 启动Spring-Security提供的权限管理功能
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends
        WebSecurityConfigurerAdapter {
    //当前类继承WebSecurityConfigurerAdapter
    // 能够重写这个父类中的方法,这个父类中的方法都是用于设置权限管理的

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService);

    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable() //禁用防跨域攻击功能
                .authorizeRequests() // 设置网站的访问及放行规则
                // 下面的方法开始指定路径
                .antMatchers(
//                        "/index_student.html",
                        "/css/*",
                        "/js/*",
                        "/img/**",
                        "/bower_components/**",
                        "/login.html",
                        "/register.html",
                        "/register")
                .permitAll() // 上面的路径是全部允许的(不需要登录就能访问)
                .anyRequest() // 除上面之外的其他路径
                .authenticated() // 需要登录才能访问
                .and()  //上面的配置完成了,开始配置下面的
                .formLogin() // 使用表单进行登录
                .loginPage("/login.html") //配置登录时显示的页面
                .loginProcessingUrl("/login") //配置处理登录的路径
                .failureUrl("/login.html?error")// 登录失败跳转的页面
                .defaultSuccessUrl("/index.html")// 登录成功跳转的页面
                .and()
                .logout()
                .logoutUrl("/logout") // 配置登出的链接
                .logoutSuccessUrl("/login.html?logout");// 登出后跳转回登录页

    }
}
