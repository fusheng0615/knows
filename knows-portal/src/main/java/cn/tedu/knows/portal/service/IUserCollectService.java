package cn.tedu.knows.portal.service;

import cn.tedu.knows.portal.model.UserCollect;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tedu.cn
 * @since 2021-10-27
 */
public interface IUserCollectService extends IService<UserCollect> {

}
