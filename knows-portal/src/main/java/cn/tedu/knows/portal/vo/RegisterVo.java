package cn.tedu.knows.portal.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

@Data
public class RegisterVo implements Serializable {

    //message就是当本属性为空时,输出的错误信息
    @NotBlank(message = "邀请码不能为空")
    private String inviteCode;  //邀请码

    @NotBlank(message = "手机号不能为空")
    @Pattern(regexp = "^1\\d{10}$",message = "手机号格式不正确")
    private String phone;       //手机号\用户名

    @NotBlank(message = "昵称不能为空")
    @Pattern(regexp = "^.{2,20}$",message = "昵称是2~20位字符")
    private String nickname;    //昵称

    @NotBlank(message = "密码不能为空")
    @Pattern(regexp = "^\\w{6,20}$",message = "密码是6~20位字符")
    private String password;    //密码

    @NotBlank(message = "确认密码不能为空")
    private String confirm;     //确认密码

}









