package cn.tedu.knows.portal.controller;


import cn.tedu.knows.portal.exception.ServiceException;
import cn.tedu.knows.portal.model.Question;
import cn.tedu.knows.portal.service.IQuestionService;
import cn.tedu.knows.portal.vo.QuestionVo;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author tedu.cn
 * @since 2021-10-27
 */
@RestController
@RequestMapping("/v1/questions")
@Slf4j
public class QuestionController {

    @Autowired
    private IQuestionService questionService;

    @GetMapping("/my")
    //@AuthenticationPrincipal注解能够将当前Spring-Security中
    // 保存的登录用户的信息取出保存到参数UserDetails中
    // Principal是当事人的意思
    public PageInfo<Question> my(
            @AuthenticationPrincipal UserDetails user,
            Integer pageNum) {
        Integer pageSize = 8;
        if (pageNum == null) {
            pageNum = 1;
        }
        PageInfo<Question> pageInfo = questionService
                .getMyQuestion(user.getUsername()
                        , pageNum, pageSize);
        return pageInfo;
    }

    @PostMapping("")
    public String createQuestion(
            @Validated QuestionVo questionVo,
            BindingResult result,
            @AuthenticationPrincipal UserDetails user) {
        log.debug("接收到表单信息:{}", questionVo);
        if (result.hasErrors()) {
            String msg = result.getFieldError()
                    .getDefaultMessage();
            return msg;
        }

        //这里调用业务逻辑层方法
        questionService.saveQuestion(questionVo, user.getUsername());
        return "ok";

    }


    // 查询讲师任务列表的控制器方法
    @GetMapping("/teacher")
    // 当前登录用户在Spring-Security登录时
    // 必须拥有/question/answer权限才能访问下面的方法
    @PreAuthorize("hasAuthority('/question/answer')")
    public PageInfo<Question> teacher(
            @AuthenticationPrincipal UserDetails user,
            Integer pageNum){
        Integer pageSize=8;
        if(pageNum==null)
            pageNum=1;
        // 调用业务逻辑层方法
        PageInfo<Question> pageInfo=questionService
                .getTeacherQuestions(user.getUsername(),
                        pageNum,pageSize);
        return pageInfo;

    }


    //根据 id 查询问题详情
    // SpringMvc支持我们编写匹配占位符的url
    // {id}就是占位符
    // 在整个控制器所有方法中,没有精确匹配时,会启动占位符匹配
    // /v1/questions/150 这个路径中,150就会匹配给{id}
    // 方法参数利用特殊注解可以获得150这个值,在方法中使用
    @GetMapping("/{id}")
    public Question question(
            // 要想获得{id}匹配的值
            // 1.必须编写@PathVariable开头
            // 2.参数名称必须和{}里的名称一致
            @PathVariable Integer id){
        Question question=questionService.getQuestionById(id);
        return question;
    }




}









