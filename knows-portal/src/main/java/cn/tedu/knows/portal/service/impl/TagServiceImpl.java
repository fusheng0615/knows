package cn.tedu.knows.portal.service.impl;

import cn.tedu.knows.portal.model.Tag;
import cn.tedu.knows.portal.mapper.TagMapper;
import cn.tedu.knows.portal.service.ITagService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author tedu.cn
 * @since 2021-10-27
 */
@Service
public class TagServiceImpl extends ServiceImpl<TagMapper, Tag> implements ITagService {

    // 缓存的使用:
    // 1.声明一个保存所有标签的属性,用于缓存所有标签
    private List<Tag> tags=new CopyOnWriteArrayList<>();
    // 2.缓存的使用逻辑:第一需要所有标签时,只能从数据库获得
    //                当第一次查询所有标签完毕后,将所有标签保存到缓存tags中
    //                第二次以后再需要所有标签,直接从缓存tags中获取

    // 声明包含所有标签的Map类型的缓存
    private Map<String,Tag> tagMap=new ConcurrentHashMap<>();



    @Autowired
    private TagMapper tagMapper;
    @Override
    public List<Tag> getTags() {
        //判断tags是不是空
        //          3
        if(tags.isEmpty()) {
            //    1    2
            synchronized (tags) {
                if(tags.isEmpty()) {
                    // 查询所有标签
                    List<Tag> tags = tagMapper
                            .selectList(null);
                    //将查询到的所有标签保存到缓存中
                    this.tags.addAll(tags);
                    //所有标签也保存在Map缓存中
                    for(Tag t:tags){
                        tagMap.put(t.getName(),t);
                    }
                }
            }
        }
        // 千万别忘了返回tags
        return tags;
    }

    @Override
    public Map<String, Tag> getTagMap() {
        // 如果当前map是空
        if(tagMap.isEmpty()){
            // 让上面的方法将map赋值
            getTags();
        }
        // 千万别忘了返回
        return tagMap;
    }
}
