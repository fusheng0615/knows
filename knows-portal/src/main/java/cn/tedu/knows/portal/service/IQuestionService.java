package cn.tedu.knows.portal.service;

import cn.tedu.knows.portal.model.Question;
import cn.tedu.knows.portal.vo.QuestionVo;
import cn.tedu.knows.portal.vo.UserVo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tedu.cn
 * @since 2021-10-27
 */
public interface IQuestionService extends IService<Question> {

    // 查询学生首页问题列表的方法
    PageInfo<Question> getMyQuestion(String username,
                          Integer pageNum, Integer pageSize);

    // 新增问题的方法(学生发布问题)
    void saveQuestion(QuestionVo questionVo,String username);


    // 查询讲师首页任务列表的方法
    PageInfo<Question> getTeacherQuestions(
             String username,Integer pageNum,Integer pageSize);

    // 根据id显示问题详情
    Question getQuestionById(Integer id);


}








