package cn.tedu.knows.portal.service.impl;

import cn.tedu.knows.portal.exception.ServiceException;
import cn.tedu.knows.portal.mapper.QuestionTagMapper;
import cn.tedu.knows.portal.mapper.UserMapper;
import cn.tedu.knows.portal.mapper.UserQuestionMapper;
import cn.tedu.knows.portal.model.*;
import cn.tedu.knows.portal.mapper.QuestionMapper;
import cn.tedu.knows.portal.service.IQuestionService;
import cn.tedu.knows.portal.service.ITagService;
import cn.tedu.knows.portal.service.IUserService;
import cn.tedu.knows.portal.vo.QuestionVo;
import cn.tedu.knows.portal.vo.UserVo;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author tedu.cn
 * @since 2021-10-27
 */
@Service
@Slf4j
public class QuestionServiceImpl extends ServiceImpl<QuestionMapper, Question> implements IQuestionService {

    @Autowired
    private QuestionMapper questionMapper;

    @Autowired
    private UserMapper userMapper;

    @Override
    public PageInfo<Question> getMyQuestion(String username,
                           Integer pageNum,Integer pageSize) {
        // 1.根据控制器提供的当前登录的用户名,查询用户信息
        User user=userMapper.findUserByUsername(username);
        // 2.根据用户id查询所有标签,这里使用QueryWrapper
        QueryWrapper<Question> query=new QueryWrapper<>();
        query.eq("user_id",user.getId());
        query.eq("delete_status",0);
        query.orderByDesc("createtime");
        //只要在执行查询前设置PageHelper分页,这次查询就会自动变为分页查询
        //startPage([页码(1表示第一页)],[每页条数])
        PageHelper.startPage(pageNum,pageSize);
        List<Question> list=questionMapper.selectList(query);
        // 循环将每个问题的标签都赋值
        for (Question q : list){
            List<Tag> tags=tagNamesToTags(q.getTagNames());
            q.setTags(tags);
        }

        // 3.别忘了返回所有问题列表
        return new PageInfo<>(list);
    }


    @Autowired
    private ITagService tagService;

    // 将tagNames字符串转换为List<Tag>的方法
    private List<Tag> tagNamesToTags(String tagNames){
        // 将"Java基础,Java SE,面试题"这样的字符串
        // 转换为{"Java基础","Java SE","面试题"}字符串数组
        String[] names=tagNames.split(",");
        // 准备包含所有标签的数组
        Map<String,Tag> tagMap=tagService.getTagMap();
        // 新实例化一个集合,用于保存获取出来的标签并返回
        List<Tag> tags=new ArrayList<>();
        // 循环遍历names
        for(String name : names){
            //获得字符串对应的标签对象
            Tag t=tagMap.get(name);
            tags.add(t);
        }
        // 别忘了返回
        return tags;
    }

    @Autowired
    private QuestionTagMapper questionTagMapper;
    @Autowired
    private UserQuestionMapper userQuestionMapper;
    @Autowired
    private IUserService userService;

    @Override
    // @Transactional(事务)
    // 下面方法中的所有增删改操作,要么都执行要么都不执行
    // 如果方法运行过程中发生异常,那么已经执行的sql操作会自动撤销(回滚)
    @Transactional
    public void saveQuestion(QuestionVo questionVo, String username) {
        // 1.查询用户名对应的用户对象
        User user=userMapper.findUserByUsername(username);
        // 2.将用户选中的所有标签的数组拼接为tagNames字符串
        // {"java基础","javaSE","面试题"}->
        //                      "Java基础,javaSE,面试题"
        StringBuilder builder=new StringBuilder();
        for(String tagName:questionVo.getTagNames()){
            builder.append(tagName).append(",");
        }
        // 去掉最后一个","并赋值给String类型变量tagNames
        String tagNames=builder
                .deleteCharAt(builder.length()-1).toString();

        // 3.实例化Question对象,收集信息,为属性赋值
        Question question=new Question()
                .setTitle(questionVo.getTitle())
                .setContent(questionVo.getContent())
                .setUserNickName(user.getNickname())
                .setUserId(user.getId())
                .setCreatetime(LocalDateTime.now())
                .setStatus(0)
                .setPageViews(0)
                .setPublicStatus(0)
                .setDeleteStatus(0)
                .setTagNames(tagNames);
        // 4.执行新增Question对象到数据库
        int num=questionMapper.insert(question);
        if(num!=1){
            throw new ServiceException("数据库忙!");
        }

        // 5.新增问题和标签的关联关系
        // 因为用户选中的是标签名称,我们需要的是标签id,
        // 所以要先获得包含所有标签的map用于根据名称获得对象
        Map<String,Tag> tagMap=tagService.getTagMap();
        // 遍历用户选中的所有标签的数组,循环新增关系
        for(String tagName:questionVo.getTagNames()){
            // 根据标签名称获得标签对象
            Tag t=tagMap.get(tagName);
            QuestionTag questionTag=new QuestionTag()
                    .setQuestionId(question.getId())
                    .setTagId(t.getId());
            num=questionTagMapper.insert(questionTag);
            if(num!=1){
                throw new ServiceException("数据库忙");
            }
            log.debug("新增了问题和标签的关系:{}",questionTag);
        }
        // 6.新增问题和讲师的关联关系
        Map<String,User> teacherMap=userService.getTeacherMap();
        for(String nickname:questionVo.getTeacherNicknames()){
            User teacher=teacherMap.get(nickname);
            UserQuestion userQuestion=new UserQuestion()
                    .setQuestionId(question.getId())
                    .setUserId(teacher.getId())
                    .setCreatetime(LocalDateTime.now());
            num=userQuestionMapper.insert(userQuestion);
            if(num!=1){
                throw new ServiceException("数据库忙");
            }
            log.debug("新增了问题和讲师的关系:{}",userQuestion);
        }

    }

    @Override
    public PageInfo<Question> getTeacherQuestions(
            String username,Integer pageNum,Integer pageSize){
        User user=userMapper.findUserByUsername(username);
        // 设置分页信息
        PageHelper.startPage(pageNum,pageSize);
        List<Question> list=questionMapper
                .findTeacherQuestions(user.getId());
        // 遍历所有问题将问题的tags属性赋值
        for(Question q:list){
            List<Tag> tags=tagNamesToTags(q.getTagNames());
            q.setTags(tags);
        }
        // 返回PageInfo类型对象
        return new PageInfo<>(list);
    }

    @Override
    public Question getQuestionById(Integer id) {
        // 查询Question数据基本信息
        Question question=questionMapper.selectById(id);
        // 为当前question对象的tags属性赋值
        List<Tag> tags=tagNamesToTags(question.getTagNames());
        question.setTags(tags);
        // 别忘了返回question
        return question;
    }


}
