package cn.tedu.knows.portal.controller;


import cn.tedu.knows.portal.model.User;
import cn.tedu.knows.portal.service.IUserService;
import cn.tedu.knows.portal.service.impl.UserServiceImpl;
import cn.tedu.knows.portal.vo.UserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author tedu.cn
 * @since 2021-10-27
 */
@RestController
// 在类上编写下面注解,表示当前控制器中的方法都需要
// 以本注解添加的路径前缀来访问
@RequestMapping("/v1/users")
public class UserController {

    @Autowired
    private IUserService userService;

    // 返回所有讲师的控制器方法
    @GetMapping("/master")
    public List<User> teachers(){
        List<User> users=userService.getTeachers();
        return users;
    }

    // 根据登录用户查询用户信息面板的方法
    @GetMapping("/me")
    public UserVo me(
            @AuthenticationPrincipal UserDetails user){
        UserVo userVo=userService.getUserVo(user.getUsername());
        return userVo;
    }





}
