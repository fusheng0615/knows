package cn.tedu.knows.portal.service.impl;

import cn.tedu.knows.portal.exception.ServiceException;
import cn.tedu.knows.portal.mapper.QuestionMapper;
import cn.tedu.knows.portal.mapper.UserMapper;
import cn.tedu.knows.portal.model.Answer;
import cn.tedu.knows.portal.mapper.AnswerMapper;
import cn.tedu.knows.portal.model.Question;
import cn.tedu.knows.portal.model.User;
import cn.tedu.knows.portal.service.IAnswerService;
import cn.tedu.knows.portal.vo.AnswerVo;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author tedu.cn
 * @since 2021-10-27
 */
@Service
public class AnswerServiceImpl extends ServiceImpl<AnswerMapper, Answer> implements IAnswerService {

    @Autowired
    private AnswerMapper answerMapper;

    @Autowired
    private UserMapper userMapper;

    @Override
    @Transactional
    public Answer saveAnswer(AnswerVo answerVo, String username) {
        User user=userMapper.findUserByUsername(username);
        Answer answer=new Answer()
                .setContent(answerVo.getContent())
                .setLikeCount(0)
                .setUserNickName(user.getNickname())
                .setUserId(user.getId())
                .setQuestId(answerVo.getQuestionId())
                .setCreatetime(LocalDateTime.now())
                .setAcceptStatus(0);
        int num=answerMapper.insert(answer);
        if(num!=1){
            throw new ServiceException("数据库忙");
        }
        // 最后别忘了返回answer
        return answer;
    }

    @Override
    public List<Answer> getAnswersByQuestionId(Integer questionId) {


        List<Answer> answers=answerMapper
                .findAnswersByQuestionId(questionId);

        // 别忘了返回answers
        return answers;
    }

    @Autowired
    private QuestionMapper questionMapper;
    @Override
    // 方法中有两次修改操作,必须添加事务支持
    @Transactional
    public boolean accept(Integer answerId, String username) {
        User user=userMapper.findUserByUsername(username);
        // 根据answerId需要查询Answer对象
        Answer answer=answerMapper.selectById(answerId);
        // 再根据answer的questId查询问题对象
        Question question=questionMapper.selectById(
                                    answer.getQuestId());
        // 判断当前用户是不是问题的发布者
        if(question.getUserId().equals(user.getId())){
            // 如果是问题的发布者在采纳答案
            // 先修改回答状态
            int num=answerMapper
                    .updateAcceptStatus(1,answerId);
            if(num!=1){
                throw new ServiceException("数据库忙");
            }
            // 再修改问题状态
            num=questionMapper
                    .updateStatus(Question.SOLVED,question.getId());
            if(num!=1){
                throw new ServiceException("数据库忙");
            }
            return true;
        }
        return false;
    }
}
