package cn.tedu.knows.portal.service;

import cn.tedu.knows.portal.model.Answer;
import cn.tedu.knows.portal.vo.AnswerVo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tedu.cn
 * @since 2021-10-27
 */
public interface IAnswerService extends IService<Answer> {


    // 新增 回答的业务逻辑层方法
    Answer saveAnswer(AnswerVo answerVo,String username);

    // 根据问题id查询所有回答
    List<Answer> getAnswersByQuestionId(Integer questionId);

    // 采纳回答的业务逻辑层
    boolean accept(Integer answerId,String username);

}







