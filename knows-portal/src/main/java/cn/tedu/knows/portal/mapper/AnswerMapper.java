package cn.tedu.knows.portal.mapper;

import cn.tedu.knows.portal.model.Answer;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tedu.cn
 * @since 2021-10-27
 */
@Repository
public interface AnswerMapper extends BaseMapper<Answer> {

    // 根据问题id查询所有回答和回答包含的所有评论的方法
    // 关联xml文件的sql语句,无需在接口中编写sql
    List<Answer> findAnswersByQuestionId(Integer questionId);


    /*
    JVM底层在编译运行时默认情况下不保存局部变量的名称,以节省内存空间
    因为参数列表也属于局部变量,所以参数的名称会在编译运行时消失
    通过SpringBoot官方脚手架创建的java项目,JVM进行参数的设置,能够保存
    局部变量的名称,所以下面的写法能正常运行
    但阿里的脚手架没有设置JVM的参数,局部变量信息仍然会丢失
    在这种情况下我们需要使用@Param注解标记对应的名称
     */
    @Update("update answer set accept_status=#{acceptStatus} " +
            "where id=#{answerId}")
    int updateAcceptStatus(
            @Param("acceptStatus") Integer acceptStatus,
            @Param("answerId") Integer answerId);

    // SSM  SSI


}
