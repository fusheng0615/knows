package cn.tedu.knows.portal.controller;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

// @RestController是处理异步请求的控制器方法
// 它所有的返回都会转换成json格式响应给axios
// 而现在我们需要根据不同身份跳转不同页面,也就是需要控制器来跳转页面
// @Controller支持我们编写返回特定格式的字符串来跳转页面
@Controller
public class HomeController {
    // 定义Spring-Security框架识别的身份常量,用于后面的方法判断身份
    public static final GrantedAuthority STUDENT=
            new SimpleGrantedAuthority("ROLE_STUDENT");
    public static final GrantedAuthority TEACHER=
            new SimpleGrantedAuthority("ROLE_TEACHER");
    // 设置 localhost:8080或localhost:8080/index.html
    // 都可以访问这个控制器
    @GetMapping(value = {"/","/index.html"})
    public String index(
            @AuthenticationPrincipal UserDetails user){
        // 判断当前用户是否包含学生角色
        if(user.getAuthorities().contains(STUDENT)){
            // 特定格式字符串跳转学生首页
            return "redirect:/index_student.html";
        }else if(user.getAuthorities().contains(TEACHER)){
            return "redirect:/index_teacher.html";
        }
        return null;
    }




}




