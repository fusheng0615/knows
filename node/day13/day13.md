# 续 迁移显示标签列表

上次课我们创建了faq项目模块

迁移了Mapper下面开始迁移业务逻辑层

## 迁移业务逻辑层

因为我们只关注标签列表

所以只迁移Tag即可

![image-20211112093942117](image-20211112093942117.png)

接口和实现类都可以通过导包解决编译错误

Tag类出现的为止都需要我们手动导包

还有一个Map

## 迁移控制层

![image-20211112094603132](image-20211112094603132.png)

这个类复制到faq之后

要讲控制器类上路径v1改为v2

```java
@RequestMapping("/v2/tags")
```

启动相关服务,访问这个路径发现需要Spring-Security的登录

## 设置Spring-Security放行

当前faq模块也需要设置全部的方法

和跨域访问的配置

可以直接从sys模块复制security包

![image-20211112095258898](image-20211112095258898.png)

复制过来是不报错的

再重启服务访问

localhost:8002/v2/tags测试

## 配置网关路由

**转到gateway模块**

添加faq模块的路由配置

application.yml文件中添加

```yaml
- id: gateway-faq
  uri: lb://faq-service
  predicates:
    - Path=/v2/**
```

启动网关后

再访问localhost:9000/v2/tags测试

## 前端项目修改请求路径

**knows-client项目**

tags_nav.js文件修改axios请求的路径

```js
axios({
    url:'http://localhost:9000/v2/tags',
    method:'GET'
})
```

启动client项目

访问学生首页

http://localhost:8080/index_student.html

如果能显示所有标签就表示一切正常

注意清除浏览器的缓存

# Redis 概述

## Redis下载

苍老师网站

![image-20211112102846589](image-20211112102846589.png)

## 什么是Redis

Redis就是一个缓存数据库

就是能够将数据保存在内存中并且提供高效查询的软件

**Redis**是一个使用ANSI C编写的开源、支持网络、基于内存、可选持久性的键值对存储数据库。目前Redis的开发由Redis Labs赞助。根据月度排行网站DB-Engines.com的数据，Redis是最流行的键值对存储数据库。

Redis也是一个软件,安装启动之后就可以使用

Redis特征

* Redis是一个内存(缓存)数据库,因为数据保存在内存中,所以速度快
  经过测试支持每秒超过10万次读写操作
* 虽然Redis是一个内存数据库,但是它支持将数据保存在硬盘上,以便在异常时恢复之前的数据内容(Redis硬盘上备份策略有两种AOF和RDB,它们可以同时开启)
* Redis使用key-value保存数据,类似java中的Map类型
  业界将这样使用key-value保存数据的数据库称之为"非关系型数据库"
  英文:"no-sql"
* Redis保存的value支持各种类型

​		string,list,set,zset,hash

* Redis支持微服务系统需要的分布式支持,以达到高并发,高可用,高性能的目的
* Redis的竞品软件:memcached

​		他们的区别可以自学

## 为什么使用Redis

![image-20211112111455949](image-20211112111455949.png)

当我们faq模块有多个项目组成集群时

每个模块都会保存一份tag缓存信息,这样就会缓存重复的信息,造成内存的浪费

我们希望可以只保存一份所有标签信息,让所有服务器使用,这就可以通过Redis实现

Redis能够节省内存,提高运行速度

## Redis的解压和安装

![image-20211112112359783](image-20211112112359783.png)

上图中redis-start.bat文件可以启动redis

出现一个dos界面,这个界面不能关,一关Redis就停了

redis-cli.exe来打开运行redis的客户端

输入info命令能够输出各种信息,表示一切正常

但是这样做并不方便,每次开机都要运行redis-start.bat还不能关窗口

我们可以用下面的办法将Redis安装在计算机中,每次开机会自动启动

下面是安装和卸载Redis相关的文件

1.service-installing.bat: 双击文件将Redis安装到当前系统

2.service-start.bat:双击文件启动Redis的服务,并每次开机自动启动

3.service-stop.bat:停止服务

4.service-uninstalling.bat:卸载服务

运行1和2文件可以安装启动Redis

每次开机自动启动Redis,可以使用redis-cli.exe文件运行进行测试

## Redis的基本使用

课程中,我们给大家介绍Redis的基本操作命令

Redis支持的几种数据类型如下

![image-20211112114151457](image-20211112114151457.png)

我们主要使用string类型的值

在Redis客户端中编写操作代码如下

```
127.0.0.1:6379> set mystr "hello world"
OK
127.0.0.1:6379> get mystr
"hello world"
```

Redis适合将标签\分类\秒杀这样频繁被访问的信息缓存起来

提高网站性能

Redis是一个单线程的软件,没有线程安全问题

因为性能足够快,所以并不会造成明显的操作延迟

除了保存和读取命令之外

还有对数字进行加减的命令

命令如下

```
127.0.0.1:6379> set num "2"
OK
127.0.0.1:6379> get num
"2"
127.0.0.1:6379> incr num
(integer) 3
127.0.0.1:6379> get num
"3"
127.0.0.1:6379> decr num
(integer) 2
```

其他的数据类型的操作在笔记文件末尾,同学们可以自行测试

# SpringBoot操作Redis

## 添加依赖

和操作mysql一样,我们要添加一些依赖

java通过JDBC可以操作mysql数据库

java通过Jedis可以操作Redis数据库

但是JDBC和Jedis操作步骤都比较繁琐

我们使用Mybatis提高操作数据库的效率

我们使用SpringBoot Redis提高操作Redis的效率

我们现在要实现标签列表保存在Redis中所以

**knows-faq模块添加依赖如下**

```xml
<dependency>
    <groupId>redis.clients</groupId>
    <artifactId>jedis</artifactId>
</dependency>
<dependency>
    <groupId>org.springframework.data</groupId>
    <artifactId>spring-data-redis</artifactId>
</dependency>
```

想数据库要配置连接信息一样

要连接Redis除了添加依赖还要在application.properties文件中

添加配置信息,代码如下

```properties
# 配置Redis的ip和端口,以便SpringDataRedis连接
spring.redis.host=localhost
spring.redis.port=6379
```

## 基础测试

先在测试类中使用方法向Redis进行存储和读取操作

代码如下

```java
// 下面的对象是从Spring容器中获得操作Redis的对象
// 而这个对象是spring-data-redis依赖提供的
// RedisTemplate<[key类型],[value]类型>
@Resource
RedisTemplate<String,String> redisTemplate;
@Test
public void redis(){
    // 新增数据到Redis
    redisTemplate.opsForValue().set("myname","欧阳锋");
    System.out.println("ok");
}
@Test
public void getValue(){
    // 读取Redis中的数据
    String name=redisTemplate.opsForValue().get("myname");
    System.out.println(name);

}
```

## Redis优化标签缓存

我们要使用Redis来保存当前项目的所有标签信息

来代替业务逻辑层中tags和tagMap属性

**knows-faq模块**TagServiceImpl实现类代码大范围修改

结果如下

```java
@Service
public class TagServiceImpl extends ServiceImpl<TagMapper, Tag> implements ITagService {

    // 操作Redis的对象
    @Resource
    private RedisTemplate<String,List<Tag>> redisTemplate;
    @Autowired
    private TagMapper tagMapper;
    @Override
    public List<Tag> getTags() {
        // 从redis中获得所有标签
        List<Tag> tags=redisTemplate.opsForValue().get("tags");
        // 判断从redis中获得所有标签是否为空
        if(tags==null){
            // 如果没有所有标签,需要连接数据库查询所有标签
            tags=tagMapper.selectList(null);
            // 新增到Redis中
            redisTemplate.opsForValue().set("tags",tags);
            System.out.println("Redis加载所有标签!");
        }
        // 返回所有标签
        return tags;
    }
    @Override
    public Map<String, Tag> getTagMap() {
        Map<String,Tag> tagMap=new HashMap<>();
        for(Tag t:getTags()){
            tagMap.put(t.getName(),t);
        }
        return tagMap;
    }
}
```

再次重启服务,我们第一次访问学生首页会出现"Redis加载所有标签!"的输出,之后再方法学生首页,就不会出现这个输出了,证明我们程序从Redis获得了所有标签列表

# 使用Ribbon实现微服务间调用

上面我们学习了注册和显示所有标签功能的迁移

下面要开始着手进行登录操作了

登录涉及很多框架及技术

Ribbon就是其中之一

## 什么是Ribbon

Ribbon也是SpringCloud提供的一个组件

它的功能是能够实现微服务之间的互相调用

使用Ribbon非常简单因为微服务中使用非常频繁

添加spring-cloud-starter-alibaba-nacos-discovery依赖时,就集成了Ribbon,Ribbon可以直接在程序中使用

## Ribbon使用示例

使用Ribbon实现调用的步骤

步骤一:

定义(明确)服务的提供者(方法的定义)

服务的提供者也叫生产者

本次调用我们将knows-sys模块作为被调用的一方

并不是随意定义任何方法都可以被Ribbon调用

必须是一个Controller控制器方法才能被调用

现在我们sys模块有/v1/auth/demo这个控制器方法,作为调用目标

步骤二:

明确发起调用的一方

这里我们让faq模块使用Ribbon调用sys模块的方法

那么faq就是这个调用中方法的消费者

要想使用Ribbon需要先添加支持Ribbon调用方法的对象保存到Spring容器中

**faq模块SpringBoot启动类**

```java
@SpringBootApplication
@EnableDiscoveryClient
@MapperScan("cn.tedu.knows.faq.mapper")
public class KnowsFaqApplication {

    public static void main(String[] args) {
        SpringApplication.run(KnowsFaqApplication.class, args);
    }

    // @Bean表示将下面方法的返回值保存到Spring容器中
    @Bean
    // 下面的注解表示这个Ribbon调用是支持负载均衡的
    @LoadBalanced
    // 下面方法返回值就是能够实现微服务互相调用的对象
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }
}
```

步骤三:

发起调用

使用faq模块保存的RestTemplate对象

在测试类中,想sys模块的/v1/auth/demo发起调用

**实际开发中一般在业务逻辑层调用其他微服务的方法**

测试代码如下

```java
@Resource
RestTemplate restTemplate;
@Test
public void ribbon(){
    //声明要调用的控制器路径
    // sys-service:要调用的微服务的模块名称(注册到Nacos的名称)
    // /v1/auth/demo: 要调用的控制方法的路径
    String url="http://sys-service/v1/auth/demo";
    // 执行Ribbon调用
    // 参数url就是上面定义的字符串
    // 参数String.class就是控制返回值的反射
    String str=restTemplate
                    .getForObject(url,String.class);
    System.out.println(str);

}
```

进行测试时必须保证Nacos和sys服务启动

faq模块没有启动的必须要求

ribbon调用示意图

![image-20211112162939842](image-20211112162939842.png)

到此为止ribbon调用完成

## 使用Ribbon实现根据用户名获得对象

我们要贴近实际开发中使用Ribbon的情形

例如我们在很多业务中需要根据用户名获得用户User对象的信息

faq模块没有这个功能,需要使用Ribbon调用

faq开始消费者(服务器的调用者)

根据上面学习的步骤,我们要先提供一个根据用户名获得用户对象的控制层方法

没有这个方法就要从业务逻辑层开始写

**knows-sys模块** IUserService接口添加方法

```java
//  根据用户名获得用户对象信息的方法
User getUserByUsername(String username);
```

UserServiceImpl实现类添加方法

```java
@Override
public User getUserByUsername(String username) {
    return userMapper.findUserByUsername(username);
}
```

AuthController添加方法

```java
@Resource
private IUserService userService;

@GetMapping("/user")
public User getUser(String username){
    return userService.getUserByUsername(username);
}
```

测试之前一定要重启修改过的Sys模块

**转到faq模块**在刚才的测试类中继续编写一个方法

```java
@Test
public void getUser(){
    // 使用Ribbon调用获得用户信息
    // url路径中?之后表示Ribbon请求的参数
    // 根据控制器实际需要参数来定义名称即可
    // 参数占位符使用{1},{2},.....
    String url=
       "http://sys-service/v1/auth/user?username={1}";
    // 调用有参数的Ribbon时
    //  从getForObject方法的第三个参数开始给{1}赋值,以此类推
    User user=restTemplate.getForObject(
            url, User.class,"st2");
    System.out.println(user);

}
```

# 微服务的会话保持

## 什么是会话保持

会话就是多次请求和响应的过程

简单来说打开浏览器访问一个网站到关闭浏览器的过程就是一次会话

会话保持指的就只在整个会话的过程中,服务器都知道当前用户的身份

单体项目使用HttpSession对象来保存会话

Spring-Security框架内部也是使用HttpSession来保存用户信息的

## 微服务项目的会话保持问题

因为微服务具有多个项目,每个服务器都有自己的内存,一个服务器中登录之后访问另外一个服务器是无法获得当前登录用户信息的

上面的问题就导致会话保持失败

![image-20211112172024371](image-20211112172024371.png)

微服务中遇到这个问题就要解决

解决方案的技术被称之为"单点登录"

## 单点登录实现思路

上面讲述了会话保持的问题和单点登录的含义

下面描述如何实现单点登录

1.Session共享

2.Token令牌

**方法一:Session共享**

原理就是让用户登录在sys模块的信息共享给所有其他模块

![image-20211112172408077](image-20211112172408077.png)

上图所示就是Session共享实现单点登录

基本思路是登录功能时将用户信息共享到Redis

谁需要这个用户信息谁去Redis中取

优点:

* 开发代码量小,难度低
* 不需要额外服务器搭建成本低

缺点

* 用户信息在内存中冗余严重,可能会影响性能
* 跨项目访问会话保持比较困难



**方法二:Token令牌**

原理是在当前登录用户登录成功时,我们将一个加密的令牌发送给客户端,由客户端保存,当客户端需要访问其他模块时,将令牌一起发送给这个模块这个模块就可以通过令牌上的信息获得用户身份

![image-20211112174733520](image-20211112174733520.png)

上图Token的解决方案

基本思路是登录成功后由客户端保存令牌

客户端需要表明身份时,将令牌发送给服务器验证即可

优点:

1.解放内存,服务器不需要再因为用户而在内存中保存数据

2.客户端保存令牌,可以访问任何识别这个令牌的项目

缺点:

1.编码流程比较复杂,有需要额外服务器的可能,成本更高

2.解析令牌和加密令牌需要CPU算力

3.令牌仍然有被仿冒的可能,安全级别高的操作需要二次严重

我们达内知道项目设计使用Token令牌完成登录功能













# 英文

no-sql:非关系型数据库

memcached:也是一个缓存数据库,课Redis功能基本一致

increment:增长

decrement:减少

Load Balanced: 负载均衡





Redis资料:

操作其他类型数据的命令

```
List 列表
常用命令: lpush,rpush,lpop,rpop,lrange等 
Redis的list在底层实现上并不是数组而是链表，Redis list 的应用场景非常多，也是Redis最重要的数据结构之一，比如微博的关注列表，粉丝列表，消息列表等功能都可以用Redis的 list 结构来实现。
Redis list 的实现为一个双向链表，即可以支持反向查找和遍历，更方便操作，不过带来了部分额外的内存开销。
另外可以通过 lrange 命令，就是从某个元素开始读取多少个元素，可以基于 list 实现分页查询，这个很棒的一个功能，基于 redis 实现简单的高性能分页，可以做类似微博那种下拉不断分页的东西（一页一页的往下走），性能高。
lists的常用操作包括LPUSH、RPUSH、LRANGE、RPOP等。可以用LPUSH在lists的左侧插入一个新元素，用RPUSH在lists的右侧插入一个新元素，用LRANGE命令从lists中指定一个范围来提取元素，RPOP从右侧弹出数据。来看几个例子：：
//新建一个list叫做mylist，并在列表头部插入元素"Tom"
127.0.0.1:6379> lpush mylist "Tom" 
//返回当前mylist中的元素个数
(integer) 1 
//在mylist右侧插入元素"Jerry"
127.0.0.1:6379> rpush mylist "Jerry" 
(integer) 2
//在mylist左侧插入元素"Andy"
127.0.0.1:6379> lpush mylist "Andy" 
(integer) 3
//列出mylist中从编号0到编号1的元素
127.0.0.1:6379> lrange mylist 0 1 
1) "Andy"
2) "Tom"
//列出mylist中从编号0到倒数第一个元素
127.0.0.1:6379> lrange mylist 0 -1 
1) "Andy"
2) "Tom"
3) "Jerry"
//从右侧取出最后一个数据
127.0.0.1:6379> rpop mylist
"Jerry"
//再次列出mylist中从编号0到倒数第一个元素
127.0.0.1:6379> lrange mylist 0 -1 
1) "Andy"
2) "Tom"
Set 集合 
常用命令： sadd,smembers,sunion 等 
set 是无序不重复集合，list是有序可以重复集合，当你需要存储一个列表数据，又不希望出现重复数据时，set是一个很好的选择，并且set提供了判断某个成员是否在一个set集合内的重要功能，这个也是list所不能提供的。
可以基于 set 轻易实现交集、并集、差集的操作。比如：在微博应用中，可以将一个用户所有的关注人存在一个集合中，将其所有粉丝存在一个集合Redis可以非常方便的实现如共同关注、共同粉丝、共同喜好等功能，也就是求交集的过程。set具体命令如下：
//向集合myset中加入一个新元素"Tom"
127.0.0.1:6379> sadd myset "Tom" 
(integer) 1
127.0.0.1:6379> sadd myset "Jerry"
(integer) 1
//列出集合myset中的所有元素
127.0.0.1:6379> smembers myset 
1) "Jerry"
2) "Tom"
//判断元素Tom是否在集合myset中，返回1表示存在
127.0.0.1:6379> sismember myset "Tom" 
(integer) 1
//判断元素3是否在集合myset中，返回0表示不存在
127.0.0.1:6379> sismember myset "Andy" 
(integer) 0
//新建一个新的集合yourset
127.0.0.1:6379> sadd yourset "Tom" 
(integer) 1
127.0.0.1:6379> sadd yourset "John"
(integer) 1
127.0.0.1:6379> smembers yourset
1) "Tom"
2) "John"
//对两个集合求并集
127.0.0.1:6379> sunion myset yourset 
1) "Tom"
2) "Jerry"
3) "John"
Sorted Set 有序集合
常用命令： zadd,zrange,zrem,zcard等 
和set相比，sorted set增加了一个权重参数score，使得集合中的元素能够按score进行有序排列。
在直播系统中，实时排行信息包含直播间在线用户列表，各种礼物排行榜，弹幕消息（可以理解为按消息维度的消息排行榜）等信息，适合使用 Redis 中的 SortedSet 结构进行存储。
很多时候，我们都将redis中的有序集合叫做zsets，这是因为在redis中，有序集合相关的操作指令都是以z开头的，比如zrange、zadd、zrevrange、zrangebyscore等等
来看几个生动的例子：
//新增一个有序集合hostset，加入一个元素baidu.com，给它赋予score:1
127.0.0.1:6379> zadd hostset 1 baidu.com 
(integer) 1
//向hostset中新增一个元素bing.com，赋予它的score是30
127.0.0.1:6379> zadd hostset 3 bing.com 
(integer) 1
//向hostset中新增一个元素google.com，赋予它的score是22
127.0.0.1:6379> zadd hostset 22 google.com 
(integer) 1
//列出hostset的所有元素，同时列出其score，可以看出myzset已经是有序的了。
127.0.0.1:6379> zrange hostset 0 -1 with scores 
1) "baidu.com"
2) "1"
3) "google.com"
4) "22"
5) "bing.com"
6) "30"
//只列出hostset的元素
127.0.0.1:6379> zrange hostset 0 -1 
1) "baidu.com"
2) "google.com"
3) "bing.com"
Hash
常用命令： hget,hset,hgetall 等。 
Hash 是一个 string 类型的 field 和 value 的映射表，hash 特别适合用于存储对象，后续操作的时候，你可以直接仅仅修改这个对象中的某个字段的值。 比如我们可以Hash数据结构来存储用户信息，商品信息等等。比如下面我就用 hash 类型存放了我本人的一些信息：
//建立哈希，并赋值
127.0.0.1:6379> HMSET user:001 username antirez password P1pp0 age 34 
OK
//列出哈希的内容
127.0.0.1:6379> HGETALL user:001 
1) "username"
2) "antirez"
3) "password"
4) "P1pp0"
5) "age"
6) "34"
//更改哈希中的某一个值
127.0.0.1:6379> HSET user:001 password 12345 
(integer) 0
//再次列出哈希的内容
127.0.0.1:6379> HGETALL user:001 
1) "username"
2) "antirez"
3) "password"
4) "12345"
5) "age"
6) "34"
 
```







# 随笔

6~8人

​	1-项目经理             25k

​	1-技术总监             25k

​    1~2-高级                20

​    2-中级                    15

​    1~2初级                  8

6~8个月

​	1~2月需求分析

​	2个月编码

​	3~4个月调试

120*2  240















