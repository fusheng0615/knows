let questionApp=new Vue({
    el:"#questionApp",
    data:{
        question:{}
    },
    methods:{
        loadQuestion:function(){
            // 获得url地址栏上?之后的内容
            let qid=location.search;
            // location.search功能:
            // 获取url中?以及之后的内容
            // 如果url中没有? qid:null
            // 如果url中有?但是?后没有任何内容 qid:null
            // 如果url中有?并且?后有内容      qid:?[之后的内容]
            //  /detail_teacher.html?150   qid:?150
            if(!qid){
                // 如果qid是null会进这个if
                alert("请指定问题id")
                return;
            }
            //  ?150 -> 150
            qid=qid.substring(1);
            // 发送axios请求
            axios({
                url:"/v1/questions/"+qid,
                method:"get"
            }).then(function(response){
                questionApp.question=response.data;
                addDuration(response.data);
            })
        }
    },
    created:function(){
        this.loadQuestion();
    }
})

let postAnswerApp=new Vue({
    el:"#postAnswerApp",
    data:{
    },
    methods:{
        postAnswer:function(){
            let qid=location.search;
            if(!qid){
                return;
            }
            qid=qid.substring(1);

            //获得用户在富文本编辑器中获得的内容
            let content=$("#summernote").val();

            //将问题id和内容保存在表单对象中
            let form=new FormData();
            form.append("questionId",qid);
            form.append("content",content);
            axios({
                url:"/v1/answers",
                method:"post",
                data:form
            }).then(function(response){
                console.log(response.data);
                let answer=response.data;
                answer.duration="刚刚";
                // 将控制新增返回的answer对象追加到回答列表中
                answersApp.answers.push(answer);
                // 富文本编辑器情况内容
                $("#summernote").summernote("reset");
            })
        }
    }
})

// 显示回答列表的js代码
let answersApp=new Vue({
    el:"#answersApp",
    data:{
        answers:[]
    },
    methods:{
        loadAnswers:function(){
            let qid=location.search;
            if(!qid){
                return;
            }
            qid=qid.substring(1);
            axios({
                url:"/v1/answers/question/"+qid,
                method:"get"
            }).then(function(response){
                answersApp.answers=response.data;
                // 调用计算持续时间的方法
                answersApp.updateDuration();
            })
        },
        updateDuration:function(){
            let answers=this.answers;
            for(let i=0;i<answers.length;i++){
                addDuration(answers[i]);
            }
        },
        postComment:function(answerId){
            // 获得用户输入的多行文本框
            let textarea=$("#addComment"+answerId+" textarea");
            let content=textarea.val();
            // 封装表单
            let form=new FormData();
            form.append("answerId",answerId)
            form.append("content",content);
            axios({
                url:"/v1/comments",
                method:"post",
                data:form
            }).then(function(response){
                console.log(response.data);
                let comment=response.data;
                let answers=answersApp.answers;
                for(let i=0;i<answers.length;i++){
                    // 遍历所有回答,
                    // 如果当前回答的id和新增评论的回答id相等
                    if(answers[i].id==answerId){
                        //将新增成功的评论添加到这个回答的评论列表中
                        answers[i].comments.push(comment);
                        textarea.val("");
                        break;
                    }
                }
            })
        },
        removeComment:function(commentId,index,comments){
            axios({
                url:"/v1/comments/"+commentId+"/delete",
                method:"get"
            }).then(function(response){
                console.log(response.data);
                comments.splice(index,1);
            })
        },
        updateComment:function(commentId,index,answer){
            // 获得修改评论的输入框和内容
            let textarea=$("#editComment"+commentId+" textarea");
            let content=textarea.val();
            // 如果没有内容 直接终止方法
            if(!content){
                return
            }
            // 表单封装CommentVo对象
            let form=new FormData();
            form.append("answerId",answer.id);
            form.append("content",content);
            axios({
                url:"/v1/comments/"+commentId+"/update",
                method:"post",
                data:form
            }).then(function(response){
                console.log(response.data)
                console.log("typeof判断返回值类型为:"
                                    +typeof(response.data))
                if(typeof(response.data)=="object") {
                    let comment = response.data;
                    // 修改操作不会影响数组元素的数量
                    // 数组元素数量不变化时,Vue不会自动更新页面上修改的内容
                    // Vue提供了修改数组元素的Api,使用这个方法修改
                    //     页面上的内容会随之变化
                    // Vue.set([修改哪个数组],[修改哪个元素],[修改成什么])
                    Vue.set(answer.comments, index, comment)
                    // 修改成功之后,编辑输入框自动折起
                    $("#editComment" + commentId).collapse("hide");
                }else{
                    alert(response.data);
                }

            })
        },
        answerSolved:function(answerId){
            axios({
                url:"/v1/answers/"+answerId+"/solved",
                method:"get"
            }).then(function(response){
                console.log(response.data);
            })
        }
    },
    created:function(){
        this.loadAnswers();
    }
})








