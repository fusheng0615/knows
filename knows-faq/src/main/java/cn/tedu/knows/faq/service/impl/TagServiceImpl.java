package cn.tedu.knows.faq.service.impl;


import cn.tedu.knows.commons.model.Tag;
import cn.tedu.knows.faq.mapper.TagMapper;
import cn.tedu.knows.faq.service.ITagService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author tedu.cn
 * @since 2021-10-27
 */
@Service
public class TagServiceImpl extends ServiceImpl<TagMapper, Tag> implements ITagService {

    // 操作Redis的对象
    @Resource
    private RedisTemplate<String,List<Tag>> redisTemplate;
    @Autowired
    private TagMapper tagMapper;
    @Override
    public List<Tag> getTags() {
        // 从redis中获得所有标签
        List<Tag> tags=redisTemplate.opsForValue().get("tags");
        // 判断从redis中获得所有标签是否为空
        if(tags==null){
            // 如果没有所有标签,需要连接数据库查询所有标签
            tags=tagMapper.selectList(null);
            // 新增到Redis中
            redisTemplate.opsForValue().set("tags",tags);
            System.out.println("Redis加载所有标签!");
        }
        // 返回所有标签
        return tags;
    }
    @Override
    public Map<String, Tag> getTagMap() {
        Map<String,Tag> tagMap=new HashMap<>();
        for(Tag t:getTags()){
            tagMap.put(t.getName(),t);
        }
        return tagMap;
    }
}
