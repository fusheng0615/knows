package cn.tedu.knows.faq.controller;


import cn.tedu.knows.commons.model.Tag;
import cn.tedu.knows.faq.service.ITagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author tedu.cn
 * @since 2021-10-27
 */
@RestController
@RequestMapping("/v2/tags")
public class TagController {

    @Autowired
    private ITagService tagService;

    // @GetMapping("")的意思是get请求,路径是
    //   localhost:8080/v1/tags
    @GetMapping("")
    public List<Tag> tags(){
        List<Tag> tags=tagService.getTags();
        return tags;
    }


}








