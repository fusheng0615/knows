package cn.tedu.knows.faq.mapper;


import cn.tedu.knows.commons.model.QuestionTag;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
* <p>
    *  Mapper 接口
    * </p>
*
* @author tedu.cn
* @since 2021-10-27
*/
    @Repository
    public interface QuestionTagMapper extends BaseMapper<QuestionTag> {

    }
