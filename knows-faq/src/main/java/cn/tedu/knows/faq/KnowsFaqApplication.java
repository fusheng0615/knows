package cn.tedu.knows.faq;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableDiscoveryClient
@MapperScan("cn.tedu.knows.faq.mapper")
public class KnowsFaqApplication {

    public static void main(String[] args) {
        SpringApplication.run(KnowsFaqApplication.class, args);
    }

    // @Bean表示将下面方法的返回值保存到Spring容器中
    @Bean
    // 下面的注解表示这个Ribbon调用是支持负载均衡的
    @LoadBalanced
    // 下面方法返回值就是能够实现微服务互相调用的对象
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }


}
