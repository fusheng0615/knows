package cn.tedu.knows.faq;

import cn.tedu.knows.commons.model.User;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

@SpringBootTest
public class RibbonTest {

    @Resource
    RestTemplate restTemplate;
    @Test
    public void ribbon(){
        //声明要调用的控制器路径
        // sys-service:要调用的微服务的模块名称(注册到Nacos的名称)
        // /v1/auth/demo: 要调用的控制方法的路径
        String url="http://sys-service/v1/auth/demo";
        // 执行Ribbon调用
        // 参数url就是上面定义的字符串
        // 参数String.class就是控制返回值的反射
        String str=restTemplate
                        .getForObject(url,String.class);
        System.out.println(str);

    }

    @Test
    public void getUser(){
        // 使用Ribbon调用获得用户信息
        // url路径中?之后表示Ribbon请求的参数
        // 根据控制器实际需要参数来定义名称即可
        // 参数占位符使用{1},{2},.....
        String url=
           "http://sys-service/v1/auth/user?username={1}";
        // 调用有参数的Ribbon时
        //  从getForObject方法的第三个参数开始给{1}赋值,以此类推
        User user=restTemplate.getForObject(
                url, User.class,"st2");
        System.out.println(user);

    }


}
