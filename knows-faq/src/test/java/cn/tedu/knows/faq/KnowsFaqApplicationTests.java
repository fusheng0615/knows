package cn.tedu.knows.faq;

import cn.tedu.knows.commons.model.Tag;
import cn.tedu.knows.faq.mapper.TagMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

import javax.annotation.Resource;
import java.util.List;

@SpringBootTest
class KnowsFaqApplicationTests {

    @Autowired
    TagMapper tagMapper;
    @Test
    void contextLoads() {
        List<Tag> tags=tagMapper.selectList(null);
        for(Tag tag:tags){
            System.out.println(tag);
        }
    }

    // 下面的对象是从Spring容器中获得操作Redis的对象
    // 而这个对象是spring-data-redis依赖提供的
    // RedisTemplate<[key类型],[value]类型>
    @Resource
    RedisTemplate<String,String> redisTemplate;
    @Test
    public void redis(){
        // 新增数据到Redis
        redisTemplate.opsForValue().set("myname","欧阳锋");
        System.out.println("ok");
    }
    @Test
    public void getValue(){
        // 读取Redis中的数据
        String name=redisTemplate.opsForValue().get("myname");
        System.out.println(name);

    }








}
