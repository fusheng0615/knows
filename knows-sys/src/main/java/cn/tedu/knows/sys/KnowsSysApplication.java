package cn.tedu.knows.sys;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
@MapperScan("cn.tedu.knows.sys.mapper")
public class KnowsSysApplication {

    public static void main(String[] args) {

        byte byteData = 127;
        byte byteData1 = byteData++;
        byte byteData2 = ++byteData;
        System.out.println(byteData1+byteData2);
        SpringApplication.run(KnowsSysApplication.class, args);
    }

}
