package cn.tedu.knows.sys.controller;



import cn.tedu.knows.commons.exception.ServiceException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

// 这个注解表示控制器有特殊情况时,可以运行这个类中的方法
// 所谓特殊情况可以多种定义,我们这里指的就是发生异常的情况
@RestControllerAdvice
@Slf4j
public class ExceptionControllerAdvice {

    // 处理ServiceException异常的方法
    // Exception:异常  Handler:处理者
    // 表示下面方法是用来处理控制器中异常的
    @ExceptionHandler
    public String handlerServiceException(ServiceException e){
        log.error("业务异常",e);
        return e.getMessage();
    }

    @ExceptionHandler
    public String handlerException(Exception e){
        log.error("其他异常",e);
        return e.getMessage();
    }
}
