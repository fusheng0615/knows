package cn.tedu.knows.sys.service.impl;


import cn.tedu.knows.commons.exception.ServiceException;
import cn.tedu.knows.commons.model.Classroom;
import cn.tedu.knows.commons.model.User;
import cn.tedu.knows.commons.model.UserRole;
import cn.tedu.knows.sys.mapper.ClassroomMapper;
import cn.tedu.knows.sys.mapper.UserMapper;
import cn.tedu.knows.sys.mapper.UserRoleMapper;
import cn.tedu.knows.sys.service.IUserService;
import cn.tedu.knows.sys.vo.RegisterVo;
import cn.tedu.knows.sys.vo.UserVo;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author tedu.cn
 * @since 2021-10-27
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

    //注入注册需要的各种依赖
    @Autowired
    private UserMapper userMapper;

    @Autowired
    private ClassroomMapper classroomMapper;

    @Autowired
    private UserRoleMapper userRoleMapper;

    @Override
    public void registerStudent(RegisterVo registerVo) {
        // 1.根据用户输入的邀请码获得班级信息
        QueryWrapper<Classroom> query=new QueryWrapper<>();
        query.eq("invite_code",registerVo.getInviteCode());
        Classroom classroom=classroomMapper.selectOne(query);
        // 2.判断班级信息是否存在,不存在直接抛异常
        if(classroom==null){
            throw new ServiceException("邀请码错误!");
        }
        // 3.根据用户输入的手机号,获得用户信息
        User user=userMapper.findUserByUsername(
                                    registerVo.getPhone());
        // 4.如果能够获得用户信息,表示当前手机号已经被注册,抛出异常
        if(user!=null){
            throw new ServiceException("手机号已经被注册!");
        }
        // 5.对用户输入的密码进行加密
        PasswordEncoder encoder=new BCryptPasswordEncoder();
        String pwd="{bcrypt}"+encoder.encode(registerVo.getPassword());
        // 6.实例化用户对象,为各个属性赋值,收集用户信息
        User u=new User()
                .setUsername(registerVo.getPhone())
                .setNickname(registerVo.getNickname())
                .setPassword(pwd)
                .setClassroomId(classroom.getId())
                .setCreatetime(LocalDateTime.now())
                .setEnabled(1)
                .setLocked(0)
                .setType(0);
        // 7.执行新增用户对象的操作
        int num=userMapper.insert(u);
        if(num!=1){
            throw new ServiceException("数据库忙");
        }
        // 8.执行新增用户角色关系表的操作
        UserRole userRole=new UserRole()
                .setUserId(u.getId())
                .setRoleId(2);
        num=userRoleMapper.insert(userRole);
        if(num!=1){
            throw new ServiceException("数据库忙");
        }

    }


    private List<User> users=new CopyOnWriteArrayList<>();
    private Map<String,User> teacherMap=
                             new ConcurrentHashMap<>();

    @Override
    public List<User> getTeachers() {
        if(users.isEmpty()) {
            synchronized (users) {
                if(users.isEmpty()) {
                    List<User> users = userMapper.findTeachers();
                    this.users.addAll(users);
                    for(User u:users){
                        teacherMap.put(u.getNickname(),u);
                    }
                }
            }
        }
        return users;
    }

    @Override
    public Map<String, User> getTeacherMap() {
        if(teacherMap.isEmpty()){
            getTeachers();
        }
        return teacherMap;
    }


    @Override
    public UserVo getUserVo(String username) {
        // 查用户信息
        User user=userMapper.findUserByUsername(username);
        // 查用户提问数

        // (作业)查询收藏数
        UserVo userVo=new UserVo()
                .setId(user.getId())
                .setUsername(user.getUsername())
                .setNickname(user.getNickname())
                ;
        // 别忘了返回!!!
        return userVo;
    }

    @Override
    public User getUserByUsername(String username) {
        return userMapper.findUserByUsername(username);
    }

}






