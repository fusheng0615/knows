package cn.tedu.knows.sys.controller;


import cn.tedu.knows.commons.model.User;
import cn.tedu.knows.sys.service.IUserService;
import cn.tedu.knows.sys.vo.RegisterVo;
import cn.tedu.knows.sys.vo.UserVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author tedu.cn
 * @since 2021-10-27
 */
@RestController
// 在类上编写下面注解,表示当前控制器中的方法都需要
// 以本注解添加的路径前缀来访问
@RequestMapping("/v1/users")
@Slf4j
public class UserController {

    @Resource
    private IUserService userService;

    // 返回所有讲师的控制器方法
    @GetMapping("/master")
    public List<User> teachers(){
        List<User> users=userService.getTeachers();
        return users;
    }

    // 根据登录用户查询用户信息面板的方法
    @GetMapping("/me")
    public UserVo me(
            @AuthenticationPrincipal UserDetails user){
        UserVo userVo=userService.getUserVo(user.getUsername());
        return userVo;
    }

    @PostMapping("/register")
    public String register(
            // RegisterVo参数前添加@Validated表示开启服务器端验证功能
            // 当控制器方法运行之前,SpringValidation框架会按RegisterVo
            // 中编写的规则进行验证
            @Validated RegisterVo registerVo,
            // 这个参数必须紧随RegisterVo之后
            // result中包含RegisterVo的验证结果
            BindingResult result) {
        //利用日志对象,将接收到的信息输出到控制台
        log.debug("接收到用户信息:{}", registerVo);
        if (result.hasErrors()) {
            String msg = result.getFieldError().
                    getDefaultMessage();
            // 返回错误信息
            return msg;
        }

        userService.registerStudent(registerVo);
        return "ok";

    }



}
