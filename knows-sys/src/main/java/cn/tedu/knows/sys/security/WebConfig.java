package cn.tedu.knows.sys.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        // 配置当前项目所有路径/**
        registry.addMapping("/**")
                .allowedOrigins("*")   // 允许任何访问源
                .allowedMethods("*")   // 允许任何方法(get\post)
                .allowedHeaders("*");  // 允许任何请求头
    }
}
