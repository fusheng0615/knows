package cn.tedu.knows.sys.service;


import cn.tedu.knows.commons.model.User;
import cn.tedu.knows.sys.vo.RegisterVo;
import cn.tedu.knows.sys.vo.UserVo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tedu.cn
 * @since 2021-10-27
 */
public interface IUserService extends IService<User> {

    // 在接口中如果想转到实现类快捷键Ctrl+Alt+B

    void registerStudent(RegisterVo registerVo);

    // 查询所有讲师的方法
    List<User> getTeachers();

    // 查询所有讲师的Map的方法
    Map<String,User> getTeacherMap();

    // 按用户名查询用户信息面板的方法
    UserVo getUserVo(String username);

    //  根据用户名获得用户对象信息的方法
    User getUserByUsername(String username);

}






